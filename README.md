# Data access with sql client
<hr>

In this project we had to write SQL scripts and build a console application manipulating data with SQL in C#.

<h2>Requirements for the project</h2>
<hr/>
<h3>Appendix A<h3>
<h4>We should create several scripts which can be run to create a database, setup some tables, add relationships and pouplate the tables.</h4>
<h5>Relationships</h5>
<ul>
<li>Create a script that contains statements to ALTER any tables needed to add the relationship between Superhero and assistent.</li>
<li>Create a script that contains statements to create the linking table.</li>
</ul>
<h5>Inserting data</h5>
<ul>
<li>Create a script that inserts three superheroes.</li>
<li>Create a script that inserts three assistants which are linked to the superheroes.</li>
<li>Create a script that inserts four powers.</li>
</ul>
<h5>Updating data</h5>
<ul>
<li>Create a script that updates a superheroes name.</li>
</ul>
<h5>Deleting data</h5>
<ul>
<li>Create a script that deletes an assistent.</li>
</ul>

<h3>Appendix B</h3>
<h4>We should manipulate SQL Server Data from the Chinook Database in Visual Studio.</h4>
<ul>
<li>Read all the customers in the database, this should display their: Id, first name, lastname, country, postal code, phone number and email.</li>
<li>Read a specific customer from the database by id.</li>
<li>Read a specific customer by name.</li>
<li>Return a page of customers by limit and offset.</li>
<li>Add a new customer to the database.</li>
<li>Update an existing customer.</li>
<li>Return the number of customers in each country, ordered by descending.</li>
<li>Return the highest spenders ordered descending.</li>
<li>Return the customers most popular genre.</li>
</ul>
