﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookDataAccessLibrary.Models;
using Microsoft.Data.SqlClient;

namespace ChinookDataAccessLibrary.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {   
        /// <summary>
        /// Selects every record in the customer table.
        /// </summary>
        /// <returns>List of customer objects.</returns>
        public IEnumerable<Customer> GetAll()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT * FROM Customer";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(7);
                                temp.PostalCode = reader[8].ToString();
                                temp.PhoneNumber = reader[9].ToString();
                                temp.Email = reader[11].ToString();
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return custList;
        }

        /// <summary>
        /// Selects a specific record by id in customer table. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A customer object.</returns>
        public Customer GetById(int id)
        {
            Customer tempCustomer = new Customer();
            string sql = "SELECT * FROM Customer WHERE CustomerId = @id";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                tempCustomer.Id = reader.GetInt32(0);
                                tempCustomer.FirstName = reader.GetString(1);
                                tempCustomer.LastName = reader.GetString(2);
                                tempCustomer.Country = reader.GetString(7);
                                tempCustomer.PostalCode = reader[8].ToString();
                                tempCustomer.PhoneNumber = reader[9].ToString();
                                tempCustomer.Email = reader[11].ToString();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return tempCustomer;
        }

        /// <summary>
        /// Adds a customer record to the customers table.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Success boolean.</returns>
        public bool Add(Customer entity)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                        command.Parameters.AddWithValue("@LastName", entity.LastName);
                        command.Parameters.AddWithValue("@Country", entity.Country);
                        command.Parameters.AddWithValue("@PostalCode", entity.PostalCode);
                        command.Parameters.AddWithValue("@Phone", entity.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", entity.Email);

                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return success;
        }

        /// <summary>
        /// Updates a specific customer record in the customers table.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Success boolean.</returns>
        public bool Update(Customer entity)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @PhoneNumber, Email = @Email WHERE CustomerId = @id";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@id", entity.Id);
                        command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                        command.Parameters.AddWithValue("@LastName", entity.LastName);
                        command.Parameters.AddWithValue("@Country", entity.Country);
                        command.Parameters.AddWithValue("@PostalCode", entity.PostalCode);
                        command.Parameters.AddWithValue("@PhoneNumber", entity.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", entity.Email);

                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return success;
        }

        /// <summary>
        /// Deletes a specific record in the customers table.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Success boolean.</returns>
        public bool Delete(int id)
        {
            bool success = false;
            string sql = "DELETE FROM Customer WHERE CustomerId = @Id";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return success;
        }

        /// <summary>
        /// Gets the last customer in the table where name matches the input name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Customer object.</returns>
        public Customer GetCustomerByName(string name)
        {
            Customer tempCustomer = new Customer();
            string sql = "SELECT * FROM Customer WHERE FirstName LIKE @FirstName";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@FirstName", '%' + name + '%');
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                tempCustomer.Id = reader.GetInt32(0);
                                tempCustomer.FirstName = reader.GetString(1);
                                tempCustomer.LastName = reader.GetString(2);
                                tempCustomer.Country = reader.GetString(7);
                                tempCustomer.PostalCode = reader[8].ToString();
                                tempCustomer.PhoneNumber = reader[9].ToString();
                                tempCustomer.Email = reader[11].ToString();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return tempCustomer;
        }

        /// <summary>
        /// Selects the customer records starting at row number equals to offset. Gets records equals to limit.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>A list of customer objects.</returns>
        public IEnumerable<Customer> GetPageOfCustomers(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT * FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@limit", limit);
                        command.Parameters.AddWithValue("@offset", offset);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(7);
                                temp.PostalCode = reader[8].ToString();
                                temp.PhoneNumber = reader[9].ToString();
                                temp.Email = reader[11].ToString();
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return custList;
        }

        /// <summary>
        /// Selects the number of the customers per country grouped by country in descending order.
        /// </summary>
        /// <returns>A list of customercountry objects.</returns>
        public IEnumerable<CustomerCountry> GetCustomersByCountry()
        {
            List<CustomerCountry> custList = new List<CustomerCountry>();
            string sql = "SELECT COUNT(CustomerID) as Count, Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.CustomerCount = reader.GetInt32(0);
                                temp.CountryName = reader.GetString(1);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return custList;
        }

        /// <summary>
        /// Selects the name and total invoice of the top 3 spending customers.
        /// </summary>
        /// <returns>A list of customerspender objects.</returns>
        public IEnumerable<CustomerSpender> GetTopSpenders()
        {
            List<CustomerSpender> custList = new List<CustomerSpender>();
            string sql = "SELECT TOP 3 Customer.CustomerId, Customer.FirstName, SUM(Invoice.Total) as Total FROM Customer INNER JOIN Invoice ON Customer.CustomerID = Invoice.CustomerID GROUP BY Customer.CustomerID, Customer.FirstName ORDER BY Total DESC";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.InvoiceTotal = (double)reader.GetDecimal(2);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return custList;
        }

        /// <summary>
        /// Selects a customers most purchased genres.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of customergenre objects.</returns>
        public IEnumerable<CustomerGenre> GetCustomerTopGenres(int id)
        {
            List<CustomerGenre> custList = new List<CustomerGenre>();
            string sql = @"SELECT TOP 1 WITH TIES Customer.CustomerId, Customer.FirstName, Genre.Name as GenreName, COUNT(Genre.Name) as GenreCount 
            FROM Customer
            INNER JOIN Invoice ON Customer.CustomerID = Invoice.CustomerID
            INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
            INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId
            INNER JOIN Genre ON Track.GenreId = Genre.GenreId
            WHERE Customer.CustomerId = @Id
            GROUP BY Genre.Name, Customer.CustomerId, Customer.FirstName
                ORDER BY GenreCount DESC";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionBuilder.BuildConnectionString()))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.GenreName = reader.GetString(2);
                                temp.GenreCount = reader.GetInt32(3);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return custList;
        }
    }
}
