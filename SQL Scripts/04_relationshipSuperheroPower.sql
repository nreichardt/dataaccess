USE SuperheroesDB
GO

CREATE TABLE [dbo].[SuperheroPower]
(
	[SuperheroId] INT NOT NULL FOREIGN KEY REFERENCES Superhero(SuperheroId),
	[PowerId] INT NOT NULL FOREIGN KEY REFERENCES Power(PowerId),
	CONSTRAINT PK_SuperheroPower PRIMARY KEY ([SuperheroId],[PowerId])
);