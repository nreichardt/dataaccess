﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookDataAccessLibrary.Models;

namespace ChinookDataAccessLibrary.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Customer GetCustomerByName(string name);
        IEnumerable<Customer> GetPageOfCustomers(int limit, int offset);
        IEnumerable<CustomerCountry> GetCustomersByCountry();
        IEnumerable<CustomerSpender> GetTopSpenders();
        IEnumerable<CustomerGenre> GetCustomerTopGenres(int id);
    }
}
