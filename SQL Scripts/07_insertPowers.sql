USE SuperheroesDB

INSERT INTO Power(Name, Description)
VALUES('FIREBALL', 'Fires a big extreme ball of fire');
INSERT INTO Power(Name, Description)
VALUES('FROSTBOLT', 'Fires a big extreme bolt of ice');
INSERT INTO Power(Name, Description)
VALUES('FIREFROSTBOLT', 'Fires a big extreme bolt of firefrost');
INSERT INTO Power(Name, Description)
VALUES('FROSTFIREBOLT', 'Fires a big extreme bolt of frostfire');

INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES(1, 1);
INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES(1, 2);
INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES(2, 3);
INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES(3, 4);
INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES(3, 1);