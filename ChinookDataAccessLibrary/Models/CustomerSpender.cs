﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookDataAccessLibrary.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public double InvoiceTotal { get; set; }
    }
}
