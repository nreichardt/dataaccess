USE [SuperheroesDB];
GO

ALTER TABLE [dbo].[Assistant]
ADD SuperheroId INT
ALTER TABLE [dbo].[Assistant]
ADD CONSTRAINT [FK_SuperheroId] FOREIGN KEY(SuperheroId) REFERENCES Superhero([SuperheroId])
